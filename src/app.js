require('dotenv').config();
const express = require('express');
const consolidate = require('consolidate');
const mongoose = require('mongoose');

mongoose.connect(process.env.MONGODB_URI, { useNewUrlParser: true });

const landingPageSchema = new mongoose.Schema({}, { strict: false});
const LandingPage = mongoose.model('LandingPage',landingPageSchema,'landing-page');

const app = express();

app.engine('html', consolidate.handlebars);
app.set('view engine', 'html');
app.set('views', __dirname + '/views');

app.use('/assets', express.static('public'));

app.get('/', async function (req, res) {
  const query = await LandingPage.findOne();
  res.render('landing-page', query.toObject());
});

app.listen('3000');


