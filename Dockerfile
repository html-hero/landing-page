FROM node:10.15.0-alpine

RUN mkdir /app

ADD package.json /app/package.json

WORKDIR /app

RUN npm install

COPY . .

ENTRYPOINT ["npm", "start"]
